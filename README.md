# Myx Nokia X2 Promo
---------------------

###Team

  * **Account Manager: Jovic** 
  * **Digital Strategist: Ashrey Del Rio** 
  * **Visual Interface Designer: Daryll Camua** 
  * **Front-end Developer: Luis Baptista** 
  * **Backend Developer: John Carlo Bernabe** 
  * **Project Manager: Debbie Medina** 

###Staging URL
[http://10.0.7.165/NokiaX2Promo/](http://10.0.7.165/NokiaX2Promo/)